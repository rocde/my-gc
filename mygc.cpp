#include <fstream>
#include <io.h>
#include <iostream>
#include <regex>
#include <vector>

using namespace std;

void mygc_get_filelist(string path, string &pathlist) {
    vector<string> files;

    long long hFile = 0;
    struct _finddata_t fileinfo;
    string p;

    if ((hFile = _findfirst(p.assign(path).append("\\*").c_str(), &fileinfo)) != -1) {
        do {
            files.push_back(p.assign(path).append("\\").append(fileinfo.name));
        } while (_findnext(hFile, &fileinfo) == 0);
        _findclose(hFile);
    }

    int size = files.size();
    for (int i = 0; i < size; i++) {
        pathlist = pathlist + files[i] + " ";
    }
}

void mygc_get_question_name(string pathlist, string &question_name) {
    // [\w-]+(?=\.cpp)
    regex pattern("[\\w-]+(?=\\.cpp)");
    smatch res;

    if (regex_search(pathlist, res, pattern)) {
        question_name = *res.begin();
    } else {
        cout << "match failed\n";
    }
}

void mygc_creat_folder(string path) {
    if (_access(path.c_str(), 0) == -1)
        mkdir(path.c_str());
}

void mygc_generate_code(string inpath, string outpath) {
    string start = "// @lc code=start";
    string end   = "// @lc code=end";

    bool vaild = false;

    ifstream infile;
    infile.open(inpath);

    ofstream outfile;
    outfile.open(outpath);

    for (string line; getline(infile, line);) {
        if (!strcmp(line.c_str(), start.c_str())) {
            vaild = true;
            continue;
        }
        if (!strcmp(line.c_str(), end.c_str())) {
            break;
        }
        if (vaild) {
            outfile << line << endl;
        }
    }
}

int main(int argc, char **argv) {
    string basepath = "D:\\Code\\LeetCode\\cpp\\";
    string outpath  = "D:\\Code\\LeetCode\\src\\cpp\\";
    string serial   = argv[1];
    string question_name;
    string pathlist;

    string src_dir = basepath + serial;

    mygc_get_filelist(src_dir, pathlist);
    mygc_get_question_name(pathlist, question_name);

    string inpath = src_dir + "\\" + question_name + ".cpp";

    string butify_serial = serial;
    while (butify_serial.length() < 3) {
        butify_serial = "0" + butify_serial;
    }

    string outdir = outpath + butify_serial + "." + question_name;

    mygc_creat_folder(outdir);

    outpath = outdir + "\\" + question_name + ".cpp";

    mygc_generate_code(inpath, outpath);

    cout << inpath << "\n"
         << outpath << "\n";

    return 0;
}
